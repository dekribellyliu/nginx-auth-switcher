from kubernetes import client, config
from flask import Flask, jsonify, request
from datetime import datetime, timezone


app = Flask(__name__)

config.load_incluster_config()
# config.load_kube_config()

server = client.CoreV1Api()
apps_v1 = client.AppsV1Api()
cmap = client.V1ConfigMap()

@app.route("/<name>")
def hello(name):
    return "Hello " + name

@app.route("/nginx-auth/patch", methods=["POST"])
def patch():
    ns = request.json["namespace"]
    auth = request.json["nginx_basic_auth"]

    deployment_name = "dev-nginx-proxy"
    
    # Configmap
    try:
        cmap.metadata = client.V1ObjectMeta(name="env-vars")
        cmap.data = {}
        cmap.data["nginx_basic_auth"] = auth
        server.patch_namespaced_config_map(
            name="env-vars",namespace=ns,
            body=cmap)
    except:
        return f"configmap env-vars not found in namespace {ns}"        

    # Deployment
    try:
        current_time = datetime.now(timezone.utc).astimezone().isoformat()
        deployment = apps_v1.read_namespaced_deployment(name=deployment_name,namespace=ns)
        deployment.spec.template.metadata = client.V1ObjectMeta(annotations={"kubectl.kubernetes.io/restartedAt": current_time})   
        apps_v1.patch_namespaced_deployment(
            name=deployment_name,
            namespace=ns,
            body=deployment)
    except:
        return f"Deployment {deployment_name} not exists in namespace {ns}"
    else:
        return "Success"

@app.route("/nginx-auth/test", methods=["GET"])
def test():
    ns = request.json["namespace"]

    deployment_name = "dev-nginx-proxy"

    # Deployment
    try:
        deployment = apps_v1.read_namespaced_deployment(name=deployment_name,namespace=ns)
        result = deployment.spec.template.spec.containers[0].image 
    except:
        return f"Deployment {deployment_name} not exists in namespace {ns}"
    else:
        return result

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0',port=80)