def slave = "${SERVICE_NAME}-slave"
podTemplate(
  label: slave,
  containers:[
    containerTemplate(
      name: "docker", image: "docker", command: "cat", ttyEnabled: "true"),
    containerTemplate(
      name: "kubectl", image: "alpine/k8s:1.18.2", command: "cat", ttyEnabled: "true")],
  volumes:[
    hostPathVolume(
      hostPath: "/var/run/docker.sock", mountPath: "/var/run/docker.sock")
  ]
){
  node(slave){
    DOCKERFILE = "Dockerfile"
    DEPLOYMENT = "kube-deployment.yaml"
    DEV_SET_REPLICA = "1"
    DOCKER_REGISTRY = "registry-intl-vpc.ap-southeast-5.aliyuncs.com"
    DOCKER_REGISTRY_NAMESPACE= "investree"
    ALI_REG_CRED_ID = "k8s-deployer"
    SERVICE_VERSION = "${currentBuild.number}"
    CONTAINER_REGISTRY = "${DOCKER_REGISTRY}/${DOCKER_REGISTRY_NAMESPACE}"
    DOCKER_IMAGE_NAME = "${SERVICE_NAME}"
    DOCKER_IMAGE_PATH = "${CONTAINER_REGISTRY}/${DOCKER_IMAGE_NAME}"
    ALI_KUBECTL_DEV_CRED_ID = "development02-kubernetes"
    ALI_KUBE_DEV_SERVER_URL = "https://10.5.1.248:6443"
    SERVICE_DEPLOYMENT = "dev-${SERVICE_NAME}"
    BASE_URL = "${SERVICE_DEPLOYMENT}.investree.tech"
    SA = "serviceAccount.yaml"
    CR = "clusterRoles.yaml"
    CRB = "clusterRolesBinding.yaml"
    INGRESS = "ingress.yaml"

    currentBuild.description = "Executed by " + currentBuild.getBuildCauses('hudson.model.Cause$UserIdCause')[0]['userId']
    
    try{
      slackSend(channel: "#${SLACK_CHANNEL}", color: "good", message: "${SERVICE_NAME} pipeline has been started \n${env.BUILD_URL}")
      stage("Pull Source Code"){

        scm_vars = checkout([
          $class: "GitSCM",
          branches: [[name: "*/${BRANCH_NAME}"]],
          userRemoteConfigs: [
            [credentialsId: "bb-ge-fe", url: "git@bitbucket.org:dekribellyliu/nginx-auth-switcher.git"]
          ]
        ])
      }

      stage("Build and Push Docker Image"){
        container("docker"){
          withDockerRegistry([
            credentialsId: "${ALI_REG_CRED_ID}",
            url: "https://${DOCKER_REGISTRY}"
          ]){
            sh "docker build --rm -t ${DOCKER_IMAGE_PATH}:${SERVICE_VERSION} -f ${DOCKERFILE} --no-cache ."
            sh "docker push ${DOCKER_IMAGE_PATH}:${SERVICE_VERSION}"
            sh "docker rmi -f \$(docker images -a ${DOCKER_IMAGE_PATH}:${SERVICE_VERSION} -q)"
          }
        }
      }

      stage("Deploy to CLuster Development"){
        container("kubectl"){
          withKubeConfig([
            credentialsId:"${ALI_KUBECTL_DEV_CRED_ID}",
            serverUrl:"${ALI_KUBE_DEV_SERVER_URL}"
          ]){

              // NS, SA, CR, CRB
              sh "kubectl create ns ${KUBERNETES_NAMESPACE} || true"
              sh "sed -i -e 's/MS_NAMESPACE/${KUBERNETES_NAMESPACE}/' ${SA}"
              sh "sed -i -e 's/MS_NAMESPACE/${KUBERNETES_NAMESPACE}/' ${CRB}"
              sh "kubectl apply -f ${SA}"
              sh "kubectl apply -f ${CR}"
              sh "kubectl apply -f ${CRB}"

              // INGRESS
              sh "sed -i -e 's/SERVICE_NAME/${SERVICE_NAME}/' ${INGRESS}"
              sh "sed -i -e 's/BASE_URL/${BASE_URL}/' ${INGRESS}"
              sh "sed -i -e 's/MS_NAMESPACE/${KUBERNETES_NAMESPACE}/' ${INGRESS}"
              sh "kubectl apply -f ${INGRESS}"

              // set other variable
              sh "sed -i -e 's/MS_NAMESPACE/${KUBERNETES_NAMESPACE}/' ${DEPLOYMENT}"
              sh "sed -i -e 's/SERVICE_NAME/${SERVICE_NAME}/' ${DEPLOYMENT}"
              sh "sed -i -e 's/MS_SET_REPLICA/${DEV_SET_REPLICA}/' ${DEPLOYMENT}"
              sh "sed -i -e 's/MS_DEPLOYMENT/${SERVICE_DEPLOYMENT}/' ${DEPLOYMENT}"

              // set image deployment
              sh "sed -i -e 's/DOCKER_REGISTRY/${DOCKER_REGISTRY}/' ${DEPLOYMENT}"
              sh "sed -i -e 's/DOCKER_REGISTRY_NAMESPACE/${DOCKER_REGISTRY_NAMESPACE}/' ${DEPLOYMENT} "
              sh "sed -i -e 's/DOCKER_IMAGE_NAME/${DOCKER_IMAGE_NAME}/' ${DEPLOYMENT}"
              sh "sed -i -e 's/SERVICE_VERSION/${SERVICE_VERSION}/' ${DEPLOYMENT}"

              //create deployment
              sh "kubectl apply -f ${DEPLOYMENT}"

              try{
                slackSend channel: "#${SLACK_CHANNEL}", color: "good", message: "Deploying ${SERVICE_NAME}-${SERVICE_VERSION} to the cluster "
                timeout(time: 10, unit: 'MINUTES'){
                  //checking rollout status
                  sh "kubectl -n ${KUBERNETES_NAMESPACE} rollout status deployment.v1.apps/${SERVICE_DEPLOYMENT}"
                }
                slackSend channel: "#${SLACK_CHANNEL}", color: "good", message: "${SERVICE_NAME}-${SERVICE_VERSION} has been *SUCCESS* deployed to cluster"
              }
              catch(e){
                slackSend channel: "#${SLACK_CHANNEL}", color: "danger", message: "${SERVICE_NAME}-${SERVICE_VERSION} *FAIL* deployed to cluster"
                sh "kubectl -n ${KUBERNETES_NAMESPACE} rollout undo deployment.v1.apps/${SERVICE_DEPLOYMENT}"
                slackSend channel: "#${SLACK_CHANNEL}", color: "danger", message: "Rollback ${SERVICE_NAME}-${SERVICE_VERSION} Successfull"
              }
            slackSend channel: "#${SLACK_CHANNEL}", color: "good", message: "https://${BASE_URL}"
          }
        }
      }
    }
    catch (e) {
      slackSend channel: "#${SLACK_CHANNEL}", color: "danger", message: "${SERVICE_NAME} pipeline ${currentBuild.number} failed"
      throw e
    }
  }
}
